import moment from 'moment'
import { Provider } from 'react-redux'
import Calendar from 'calendar/Calendar'
import { store } from 'store'

function App() {
  return (
    <Provider store={store}>
      <div className="App">
        <Calendar currentMonth={moment().month()} />
      </div>
    </Provider>
  )
}

export default App
