import { configureStore } from "@reduxjs/toolkit"
import { combineReducers } from "redux"
import calendarSlice from "calendar/calendarSlice"

const rootReducer = combineReducers({
    calendar: calendarSlice
})

export const store = configureStore({reducer: rootReducer})