import { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { Swiper, SwiperSlide } from 'swiper/react'
import { getMonthGoals, setMonthGoal } from './calendarSlice'
import Table from './components/Table'
import { MONTHS } from './const'
import 'swiper/swiper.scss'
import styles from './Calendar.module.scss'
import Metrics from './components/Metrics'

export default ({currentMonth}) => {
    const [selectedMonth, setSelectedMonth] = useState(currentMonth)
    const [swiper, setSwiper] = useState(null)
    const monthsGoals = useSelector(state => state.calendar.monthsGoals)
    const metrics = useSelector(state => state.calendar.metrics)
    const dispatch = useDispatch()

    useEffect(() => {
        dispatch(getMonthGoals(selectedMonth))
    }, [selectedMonth])

    const onToggleWeekDay = day => {
        dispatch(setMonthGoal({ month: day.month(), day: day.date(), value: !monthsGoals[day.month()][day.date()] }))
    }

    return (
        <div className={`pt-4 ${styles['calendar']}`}>
            <div className={`d-flex justify-content-between px-5 pb-3`}>
                <button type="button" onClick={() => { swiper.slidePrev() }} className={styles['slider-control--prev']}></button>
                <div className={styles['slider-header__title']}>{MONTHS[selectedMonth]}</div>
                <button type="button" onClick={() => { swiper.slideNext() }} className={styles['slider-control--next']}></button>
            </div>
            <Swiper
                initialSlide={selectedMonth}
                onSwiper={setSwiper}
                onSlideChange={function () {
                    setSelectedMonth(this.activeIndex)
                }}
            >
                {monthsGoals.map((monthGoals, month) => <SwiperSlide key={month}>{monthGoals ? <Table month={month} monthGoals={monthGoals} onToggleWeekDay={onToggleWeekDay} /> : 'suspend'}</SwiperSlide>)}
            </Swiper>
            <Metrics metrics={metrics} />
        </div>
    )
}