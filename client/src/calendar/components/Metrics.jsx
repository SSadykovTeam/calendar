import styles from "./Metrics.module.scss"

export default ({ metrics }) => (
    <div className="row px-5">
        <div className="col-3 text-center">
            <Metric name="SUCCESS RATE" value={`${metrics.successRate}%`} />
        </div>
        <div className="col-3 text-center">
            <Metric name="GOALS MET" value={metrics.goalsMet} />
        </div>
        <div className="col-3 text-center">
            <Metric name="CURRENT STREAK" value={metrics.currentStreak} />
        </div>
        <div className="col-3 text-center">
            <Metric name="BEST STREAK" value={metrics.bestStreak} />
        </div>
    </div>
)

const Metric = ({ name, value }) => (
    <div className="d-inline-flex m-auto text-left">
        <div className={styles['metric-item__value']}>{value}</div>
        <div className={`pl-2 align-self-center ${styles['metric-item__name']}`}>{name}</div>
    </div>
)