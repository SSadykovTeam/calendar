import moment from 'moment'
import classNames from 'classnames'
import { DAYS_OF_WEEEK } from '../const'
import styles from './Table.module.scss'

export default ({ month, monthGoals, onToggleWeekDay }) => {
    const fDayOfMonth = moment().month(month).startOf('month')
    const dateFrom = fDayOfMonth.clone().startOf('week')
    const dateTo = fDayOfMonth.clone().endOf('month').endOf('week')
    const weeks = []

    const loopDate = dateFrom.clone().subtract(1, 'day')
    while (loopDate.isBefore(dateTo, 'day')) {
        weeks.push([loopDate.week(), Array(7).fill(0).map(() => loopDate.add(1, 'day').clone())])
    }

    return (
        <table className="table table-borderless">
            <thead>
                <tr>
                    {DAYS_OF_WEEEK.map(d => <th key={d} className="text-center font-weight-normal py-2">{d}</th>)}
                </tr>
            </thead>
            <tbody>
                {weeks.map(([week, days]) => (
                    <tr key={week} className="text-center">
                        {days.map(day => (
                            <td key={day.date()} className="py-1">
                                <button type="button"
                                    disabled={day.month() != month}
                                    onClick={() => { onToggleWeekDay(day) }}
                                    className={
                                        classNames({
                                            [styles['week-day']]: true,
                                            [styles['week-day--current']]: day.month() == month,
                                            [styles['week-day--selected']]: day.month() == month && !!monthGoals[day.date()]
                                        })
                                    }>
                                    {day.format('D')}
                                </button>
                            </td>
                        ))}
                    </tr>
                ))}
            </tbody>
        </table >
    )
}