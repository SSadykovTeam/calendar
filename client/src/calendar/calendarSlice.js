import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'
import HttpClient from '../foundation/lib/HttpClient'
import { API } from './const'

const initialState = {
    monthsGoals: Array(12).fill(null),
    metrics: {
        successRate: 0,
        goalsMet: 0,
        currentStreak: 0,
        bestStreak: 0,
    }
}

export const getMonthGoals = createAsyncThunk(
    'calendar/getMonthGoals',
    async (month) => {
        const response = await HttpClient.get(API.GOALS + month)
        return response.data
    }
)

export const setMonthGoal = createAsyncThunk(
    'calendar/setMonthGoal',
    async ({ month, day, value }) => {
        const response = await HttpClient.post(API.GOALS + month, { day, value })
        return response.data
    }
)

const calendarSlice = createSlice({
    name: 'calendar',
    initialState,
    extraReducers: {
        [getMonthGoals.fulfilled]: (state, { payload: { month, monthGoals, metrics } }) => {
            state.monthsGoals[month] = monthGoals
            state.metrics = metrics
        },
        [setMonthGoal.fulfilled]: (state, { payload: { month, monthGoals, metrics } }) => {
            state.monthsGoals[month] = monthGoals
            state.metrics = metrics
        }
    }
})

export default calendarSlice.reducer