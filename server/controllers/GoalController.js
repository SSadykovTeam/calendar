const moment = require('moment')
const MetricaService = require('../services/MetricaService')

class GoalController {
    constructor() {
        this.metricaService = new MetricaService()
        this.monthsGoals = Array(12).fill(0).map(() => ({}))

        this.show = this.show.bind(this)
        this.store = this.store.bind(this)
    }
    show(req, res) {
        res.json({
            sucess: true,
            month: this.getMonth(req),
            monthGoals: this.monthsGoals[this.getMonth(req)] ?? {},
            metrics: this.metricaService.calculate(this.monthsGoals, moment().month(this.getMonth(req)))
        })
    }
    store(req, res) {
        this.monthsGoals[this.getMonth(req)][parseInt(req.body.day)] = !!req.body.value
        this.show(req, res)
    }

    getMonth(req) {
        return parseInt(req.params.month)
    }
}

module.exports = new GoalController