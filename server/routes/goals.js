var express = require('express');
var router = express.Router();
const GoalController = require('../controllers/GoalController')

router.get('/:month', GoalController.show);

router.post('/:month', GoalController.store);

module.exports = router;
