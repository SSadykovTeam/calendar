const moment = require('moment')

class MetricaService {
    calculate(monthsGoals, momentMonth) {
        const monthGoals = monthsGoals[momentMonth.month()] ?? {}
        return {
            successRate: this.#calculateSuccessRate(monthGoals, momentMonth),
            goalsMet: this.#calculateGoalsMet(monthGoals),
            currentStreak: this.#calculateCurrentStreak(monthGoals, momentMonth),
            bestStreak: this.#calculateBestStreak(monthsGoals, momentMonth),
        }
    }

    #calculateSuccessRate(monthGoals, momentMonth) {
        return Math.ceil(this.#calculateGoalsMet(monthGoals) / momentMonth.daysInMonth() * 100)
    }

    #calculateGoalsMet(monthGoals) {
        return Object.values(monthGoals).filter(Boolean).length
    }

    #calculateCurrentStreak(monthGoals, momentMonth) {
        let streak = 0
        for (let i = 1; i <= momentMonth.daysInMonth(); i++) {
            if (!monthGoals[i]) break
            streak++
        }
        return streak
    }

    #calculateBestStreak(monthsGoals, momentMonth) {
        const monthsStreak = []

        for (let i = 0; i < 12; i++) {
            monthsStreak[i] = this.#calculateCurrentStreak(monthsGoals[i], momentMonth.clone().month(i))
        }

        return Math.max(...monthsStreak)
    }

}

module.exports = MetricaService